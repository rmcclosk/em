#include <check.h>
#include <stdlib.h>
#include "../src/tree.h"

Suite *tree_suite(void);

const char *tree_3 = "(v1:0.5,(v3:0.5,v4:0.25)v2:0.25)v0;";

START_TEST (test_parse)
{
    FILE *tmp = tmpfile();
    em_tree *tree;
    fprintf(tmp, "%s", tree_3);
    fseek(tmp, 0, SEEK_SET);

    tree = em_tree_parse(tmp);
    ck_assert(em_child(tree, 0, 0) == -1);
    ck_assert(em_child(tree, 0, 1) == -1);
    ck_assert(em_child(tree, 1, 0) == -1);
    ck_assert(em_child(tree, 1, 1) == -1);
    ck_assert(em_child(tree, 2, 0) == -1);
    ck_assert(em_child(tree, 2, 1) == -1);
    ck_assert(em_child(tree, 3, 0) == 1);
    ck_assert(em_child(tree, 3, 1) == 2);
    ck_assert(em_child(tree, 4, 0) == 0);
    ck_assert(em_child(tree, 4, 1) == 3);
    ck_assert(em_branch_length(tree, 0) == 0.5);
    ck_assert(em_branch_length(tree, 1) == 0.5);
    ck_assert(em_branch_length(tree, 2) == 0.25);
    ck_assert(em_branch_length(tree, 3) == 0.25);
    ck_assert(em_branch_length(tree, 4) == 0);

    fclose(tmp);
    em_tree_free(tree);
}
END_TEST

Suite *tree_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("tree");

    tc_core = tcase_create("Core");
    tcase_add_test(tc_core, test_parse);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = tree_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return number_failed;
}
