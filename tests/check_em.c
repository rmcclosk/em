#include <check.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <cblas.h>
#include "../src/em.h"

#define EPSILON 1e-6

Suite *em_suite(void);

const char *tree_3 = "(v1:0.5,(v3:0.5,v4:0.25)v2:0.25)v0;";

em_tree *make_tree(void);
em_workspace *make_workspace(void);

em_tree *make_tree(void)
{
    FILE *tmp = tmpfile();
    em_tree *tree;

    fprintf(tmp, "%s", tree_3);
    fseek(tmp, 0, SEEK_SET);
    tree = em_tree_parse(tmp);
    fclose(tmp);

    return tree;
}

em_workspace *make_workspace(void)
{
    em_workspace *w = em_workspace_create(2, 5);
    double lambda[2] = {1, 2};
    double q[2] = {0.5, 0.25};
    em_set_parameters(w, lambda, q);
    return w;
}

START_TEST (test_set_parameters)
{
    em_workspace *w = em_workspace_create(2, 5);
    double lambda[2] = {1, 2};
    double q[2] = {0.5, 0.25};
    em_set_parameters(w, lambda, q);
    ck_assert(em_get_branching_rate(w, 0) == 1);
    ck_assert(em_get_branching_rate(w, 1) == 2);
    ck_assert(em_get_transition_rate(w, 0, 0) == -0.5);
    ck_assert(em_get_transition_rate(w, 0, 1) == 0.5);
    ck_assert(em_get_transition_rate(w, 1, 0) == 0.25);
    ck_assert(em_get_transition_rate(w, 1, 1) == -0.25);
    em_workspace_free(w);
}
END_TEST

START_TEST (test_calculate_SD)
{
    em_workspace *w = em_workspace_create(3, 5);
    int i, j, n = 3;
    double lambda[3] = {0, 0, 0};
    double q[6] = {0.5, 0.25, 1, 0.2, 0.1, 0.8};
    double D[9], S[9], Sinv[9], tmp1[9], tmp2[9];

    em_set_parameters(w, lambda, q);

    // retrieve S, D, S^-1 from the workspace
    memset(D, 0, n * n * sizeof(double));
    for (i = 0; i < n; ++i) {
        D[i * n + i] = em_get_D(w, i);
        for (j = 0; j < n; ++j) {
            S[i * n + j] = em_get_S(w, i, j);
            Sinv[i * n + j] = em_get_Sinv(w, i, j);
        }
    }

    // multiply S * D * S^-1
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1.0, S,
                n, D, n, 0.0, tmp1, n);
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1.0, tmp1,
                n, Sinv, n, 0.0, tmp2, n);

    // make sure the result is equal to the original Q - Lambda
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j)
            ck_assert(fabs(tmp2[i * n + j] - em_get_transition_rate(w, i, j)) 
                      < EPSILON);
    }

    em_workspace_free(w);
}
END_TEST

START_TEST (test_calculate_pi)
{
    em_workspace *w = make_workspace();

    ck_assert(fabs(em_get_pi(w, 0) - 0.2) < EPSILON);
    ck_assert(fabs(em_get_pi(w, 1) - 0.8) < EPSILON);

    em_workspace_free(w);
}
END_TEST

START_TEST (test_calculate_f)
{
    em_tree *t = make_tree();
    em_workspace *w = make_workspace();
    em_step(w, t);

    ck_assert(fabs(em_get_f(w, 0, 0, 0) - 0.478922) < EPSILON);
    ck_assert(fabs(em_get_f(w, 0, 0, 1) - 0.098989) < EPSILON);
    ck_assert(fabs(em_get_f(w, 0, 1, 0) - 0.049494) < EPSILON);
    ck_assert(fabs(em_get_f(w, 0, 1, 1) - 0.330439) < EPSILON);

    ck_assert(fabs(em_get_f(w, 4, 0, 0) - 1) < EPSILON);
    ck_assert(fabs(em_get_f(w, 4, 0, 1) - 0) < EPSILON);
    ck_assert(fabs(em_get_f(w, 4, 1, 0) - 0) < EPSILON);
    ck_assert(fabs(em_get_f(w, 4, 1, 1) - 2) < EPSILON);

    em_tree_free(t);
    em_workspace_free(w);
}
END_TEST

START_TEST (test_forward)
{
    em_tree *t = make_tree();
    em_workspace *w = make_workspace();
    em_step(w, t);

    ck_assert(fabs(em_get_L(w, 1, 0) - 0.127963) < EPSILON);
    ck_assert(fabs(em_get_L(w, 1, 1) - 0.329631) < EPSILON);
    
    em_tree_free(t);
    em_workspace_free(w);
}
END_TEST

START_TEST (test_backward)
{
    em_tree *t = make_tree();
    em_workspace *w = make_workspace();
    em_step(w, t);

    ck_assert(fabs(em_get_R(w, 0, 0) - 0.577910) < EPSILON);
    ck_assert(fabs(em_get_R(w, 0, 1) - 0.379933) < EPSILON);

    ck_assert(fabs(em_get_R(w, 3, 0) - 0.286806) < EPSILON);
    ck_assert(fabs(em_get_R(w, 3, 1) - 0.333801) < EPSILON);
    
    em_tree_free(t);
    em_workspace_free(w);
}
END_TEST

START_TEST (test_calculate_A)
{
    em_tree *t = make_tree();
    em_workspace *w = make_workspace();
    em_step(w, t);

    fprintf(stderr, "%f\t", em_get_A(w, 0, 0));
    fprintf(stderr, "%f\n", em_get_A(w, 0, 1));
    fprintf(stderr, "%f\t", em_get_A(w, 1, 0));
    fprintf(stderr, "%f\n", em_get_A(w, 1, 1));
    ck_assert(0);
    
    em_tree_free(t);
    em_workspace_free(w);
}
END_TEST

Suite *em_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("em");

    tc_core = tcase_create("Core");
    /*
    tcase_add_test(tc_core, test_set_parameters);
    tcase_add_test(tc_core, test_calculate_SD);
    tcase_add_test(tc_core, test_calculate_pi);
    tcase_add_test(tc_core, test_calculate_f);
    tcase_add_test(tc_core, test_forward);
    tcase_add_test(tc_core, test_backward);
    */
    tcase_add_test(tc_core, test_calculate_A);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = em_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return number_failed;
}
