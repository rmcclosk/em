#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "newick_parser.h"
#include "tree.h"

void yyrestart(FILE *f);

struct em_tree {
    int nnode;
    int *children;
    int *parent;
    double *branch_length;
};

em_tree *em_tree_parse(FILE *f)
{
    em_tree *tree = malloc(sizeof(em_tree));
    char buf[BUFSIZ];
    char *cur;
    int *size;
    int i;

    tree->nnode = 0;
    while (fgets(buf, BUFSIZ, f) != NULL) {
        cur = buf;
        while ((cur = strpbrk(cur, ",);")) != NULL) {
            ++tree->nnode;
            ++cur;
        }
    }

    tree->children = malloc(2 * tree->nnode * sizeof(int));
    tree->parent = malloc(tree->nnode * sizeof(int));
    tree->branch_length = calloc(tree->nnode, sizeof(double));
    size = calloc(tree->nnode, sizeof(int));

    fseek(f, 0, SEEK_SET);
    yyrestart(f);

    yyparse(tree->children, size, tree->branch_length);
    free(size);

    tree->parent[tree->nnode-1] = -1;
    for (i = 0; i < tree->nnode; ++i) {
        if (tree->children[2*i] != -1) {
            tree->parent[tree->children[2*i]] = i;
            tree->parent[tree->children[2*i+1]] = i;
        }
    }
    return tree;
}

int em_root(const em_tree *tree)
{
    return tree->nnode - 1;
}

int em_nnode(const em_tree *tree)
{
    return tree->nnode;
}

int em_is_tip(const em_tree *tree, int node)
{
    return tree->children[2*node] == -1;
}

int em_child(const em_tree *tree, int node, int right)
{
    return tree->children[2*node+right];
}

int em_parent(const em_tree *tree, int node)
{
    return tree->parent[node];
}

double em_branch_length(const em_tree *tree, int node)
{
    return tree->branch_length[node];
}

void em_tree_free(em_tree *tree)
{
    free(tree->children);
    free(tree->branch_length);
    free(tree->parent);
    free(tree);
}
