#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <lapacke.h>
#include <cblas.h>
#include <float.h>
#include "em.h"
#include "tree.h"
#include "util.h"

void em_calculate_SD(em_workspace *w);
void em_calculate_pi(em_workspace *w);
void em_calculate_f(em_workspace *w, em_tree *t);
void em_forward(em_workspace *w, em_tree *t);
void em_backward(em_workspace *w, em_tree *t);
void em_A_integral(em_workspace *w, double *Fint, int i, int j, double delta_t);
void em_calculate_A(em_workspace *w, em_tree *t);
void em_calculate_B(em_workspace *w, em_tree *t);
void em_reestimate(em_workspace *w);

struct em_workspace {
    int nstate;
    int nnode;

    double *Q;
    double *Lambda;
    double *S;
    double *Sinv;
    double *D;
    double *pi;

    double *f;
    double *L;
    double *R;
    double *A;
    double *B;

    int *f_exp;
    int *L_exp;
    int *R_exp;
    int *A_exp;
    int *B_exp;

    double *tmpd;
    int *tmpi;
};

em_workspace *em_workspace_create(int nstate, int nnode)
{
    em_workspace *w = malloc(sizeof(em_workspace));

    w->nstate = nstate;
    w->nnode = nnode;

    w->Q = calloc(nstate * nstate, sizeof(double));
    w->Lambda = calloc(nstate * nstate, sizeof(double));
    w->S = calloc(nstate * nstate, sizeof(double));
    w->Sinv = calloc(nstate * nstate, sizeof(double));
    w->D = calloc(nstate * nstate, sizeof(double));
    w->pi = calloc(nstate, sizeof(double));

    w->L = malloc(nnode * nstate * sizeof(double));
    w->R = malloc(nnode * nstate * sizeof(double));
    w->f = malloc(nnode * nstate * nstate * sizeof(double));
    w->A = malloc(nstate * nstate * sizeof(double));
    w->B = malloc(nstate * sizeof(double));

    w->f_exp = calloc(nnode, sizeof(int));
    w->L_exp = calloc(nnode, sizeof(int));
    w->R_exp = calloc(nnode, sizeof(int));
    w->A_exp = calloc(nstate * nstate, sizeof(int));
    w->B_exp = calloc(nstate, sizeof(int));

    w->tmpd = calloc(nstate * nstate * 4, sizeof(double));
    w->tmpi = calloc(nstate * nstate, sizeof(int));
    return w;
}

void em_set_parameters(em_workspace *w, const double *lambda, const double *q)
{
    int i, j, n = w->nstate, cur = 0;
    double rowsum;

    memset(w->Lambda, 0, n * n * sizeof(double));
    for (i = 0; i < n; ++i) {
        w->Lambda[i * n + i] = lambda[i];
        rowsum = 0;
        for (j = 0; j < n; ++j) {
            if (i != j) {
                w->Q[i * n + j] = q[cur++];
                rowsum += w->Q[i * n + j];
            }
        }
        w->Q[i * n + i] = -rowsum;
    }
    em_calculate_SD(w);
    em_calculate_pi(w);
}

void em_calculate_SD(em_workspace *w)
{
    int i, n = w->nstate;
    for (i = 0; i < n * n; ++i)
        w->tmpd[i] = w->Q[i] - w->Lambda[i];

    LAPACKE_dgeev(LAPACK_ROW_MAJOR, 'N', 'V', n, w->tmpd, n, w->D, &w->D[n], 
                  NULL, n, w->S, n);
    for (i = 0; i < n; ++i) {
        if (w->D[n + i] != 0) {
            fprintf(stderr, "warning: (Q - Lambda) has imaginary eigenvalues\n");
            fprintf(stderr, "         the imaginary part will be ignored\n");
        }
    }

    for (i = 1; i < n; ++i) {
        memset(&w->D[i*n], 0, n * sizeof(double));
        w->D[i * n + i] = w->D[i];
    }

    memcpy(w->Sinv, w->S, n * n * sizeof(double));
    LAPACKE_dgetrf(LAPACK_ROW_MAJOR, n, n, w->Sinv, n, w->tmpi);
    LAPACKE_dgetri(LAPACK_ROW_MAJOR, n, w->Sinv, n, w->tmpi);
}

void em_calculate_pi(em_workspace *w)
{
    int i, j, n = w->nstate;
    double *Pinv = w->tmpd;
    double *wr = &w->tmpd[n * n];
    double *wi = &w->tmpd[n * (n+1)];
    double *vl = &w->tmpd[n * (n+2)];
    double sum;

    for (i = 0; i < n; ++i) {
        if (w->Lambda[i * n + i] == 0) {
            fprintf(stderr, "warning: Lambda is not invertible. ");
            fprintf(stderr, "Using pseudo-inverse instead\n");
            break;
        }
    }

    for (i = 0; i < n; ++i) {
        if (w->Lambda[i * n + i] != 0) {
            for (j = 0; j < n; ++j)
                Pinv[i * n + j] = (w->Lambda[i * n + j] - w->Q[i * n + j]) / 
                                   w->Lambda[i * n + i];
        } else {
            memset(&Pinv[i*n], 0, n * sizeof(double));
        }
    }
    LAPACKE_dgeev(LAPACK_ROW_MAJOR, 'V', 'N', n, Pinv, n, wr, wi, vl, n, NULL,
                  n);

    for (i = 0; i < n; ++i) {
        if (wi[i] != 0) {
            fprintf(stderr, "warning: P^-1 has imaginary eigenvalues\n");
            fprintf(stderr, "         the imaginary part will be ignored\n");
        }
    }

    for (j = 0; fabs(wr[j] - 1) > 1e-5; ++j);
    sum = cblas_dasum(n, &vl[j], n);
    for (i = 0; i < n; ++i)
        w->pi[i] = fabs(vl[i * n + j]) / sum;
}

void em_step(em_workspace *w, em_tree *t)
{
    em_calculate_f(w, t);
    em_forward(w, t);
    em_backward(w, t);
    em_calculate_A(w, t);
    em_calculate_B(w, t);
    em_reestimate(w);
}

void em_calculate_f(em_workspace *w, em_tree *t)
{
    int i, node, n = w->nstate;
    double *expDt = w->tmpd;
    double *SexpDt = &w->tmpd[n * n];
    double *SexpDtSinv = &w->tmpd[2 * n * n];
    double delta_t, *f;
    int *f_exp = w->f_exp;

    memset(expDt, 0, n * n * sizeof(double));
    for (node = 0; node < w->nnode; ++node) {
        delta_t = em_branch_length(t, node);
        f = &w->f[node * n * n];

        for (i = 0; i < n; ++i)
            expDt[i * n + i] = exp(w->D[i * n + i] * delta_t);
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1.0,
                    w->S, n, expDt, n, 0.0, SexpDt, n);
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1.0,
                    SexpDt, n, w->Sinv, n, 0.0, SexpDtSinv, n);
        if (em_is_tip(t, node)) {
            memcpy(f, SexpDtSinv, n * n * sizeof(double));
        } else {
            cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1.0,
                        SexpDtSinv, n, w->Lambda, n, 0.0, f, n);
        }
        f_exp[node] = scale(n * n, f);
    }
}

void em_forward(em_workspace *w, em_tree *t)
{
    int node, i, child, nnode = w->nnode, n = w->nstate;
    int *L_exp = w->L_exp;
    int *f_exp = w->f_exp;

    memcpy(&w->L[n * (nnode - 1)], w->pi, n * sizeof(double));
    w->L_exp[nnode-1] = 0;
    for (node = nnode-1; node >= 0; --node) {
        if (!em_is_tip(t, node)) {
            for (i = 0; i < 2; ++i) {
                child = em_child(t, node, i);
                cblas_dgemv(CblasRowMajor, CblasTrans, n, n, 1.0, 
                            &w->f[child * n * n], n, &w->L[n * node], 1, 0.0, 
                            &w->L[child * n], 1);
                L_exp[child] = f_exp[child] + L_exp[node];
                L_exp[child] += scale(n, &w->L[child * n]);
            }
        }
    }
}

void em_backward(em_workspace *w, em_tree *t)
{
    int i, node, nnode = w->nnode, n = w->nstate;
    double *Rleft = w->tmpd;
    double *Rright = &w->tmpd[n];
    double *Rend = Rleft;
    double *f, *R;
    int *R_exp = w->R_exp;
    int *f_exp = w->f_exp;

    for (node = 0; node < nnode; ++node) {
        R = &w->R[node * n];
        f = &w->f[node * n * n];
        if (em_is_tip(t, node)) {
            for (i = 0; i < n; ++i)
                Rend[i] = 1;
            cblas_dgemv(CblasRowMajor, CblasNoTrans, n, n, 1.0, f, n, Rend, 1,
                        0.0, R, 1);
            R_exp[node] = scale(n, R);
        } else {
            cblas_dgemv(CblasRowMajor, CblasNoTrans, n, n, 1.0, f, n,
                        &w->R[em_child(t, node, 0) * n], 1, 0.0, Rleft, 1);
            cblas_dgemv(CblasRowMajor, CblasNoTrans, n, n, 1.0, f, n,
                        &w->R[em_child(t, node, 1) * n], 1, 0.0, Rright, 1);
            hadamard(n, Rleft, Rright, 1.0, R);
            R_exp[node] = 2 * f_exp[node] + R_exp[em_child(t, node, 0)] +
                          R_exp[em_child(t, node, 1)];
            R_exp[node] += scale(n, R);
        }
    }
}

void em_calculate_A(em_workspace *w, em_tree *t)
{
    int i, j, node, right, n = w->nstate;
    int nnode = w->nnode;
    double *Fint = w->tmpd;
    double *A = w->A;
    int *L_exp = w->L_exp;
    int *R_exp = w->R_exp;
    int *A_exp = w->A_exp;
    double *ones = &w->tmpd[n*n];
    double *LFint = &w->tmpd[n*(n+1)];
    double *L, *R;
    double delta_t, new_A;
    int Fscale, new_scale;
    
    memset(A, 0, n * n * sizeof(double));
    memset(A_exp, 0, n * n * sizeof(int));
    fill(n, ones, 1);

    for (node = 0; node < nnode-1; ++node) {
        delta_t = em_branch_length(t, node);
        L = &w->L[em_parent(t, node) * n];
        for (i = 0; i < n; ++i) {
            for (j = 0; j < n; ++j) {
                em_A_integral(w, Fint, i, j, delta_t);
                Fscale = scale(n * n, Fint);

                for (right = 0; right < 2; ++right) {
                    if (em_is_tip(t, node)) {
                        if (!right) R = ones; else continue;
                    } else {
                        R = &w->R[em_child(t, node, right) * n];
                    }
                    cblas_dgemv(CblasRowMajor, CblasTrans, n, n, 1.0, Fint, n,
                            L, 1, 0.0, LFint, 1);

                    new_scale = L_exp[em_parent(t, node)] + Fscale +
                                R_exp[em_child(t, node, right)];
                    new_A = cblas_ddot(n, LFint, 1, R, 1);
                    add_scale(&A[i * n + j], &A_exp[i * n + j], new_A, new_scale);
                }
            }
        }
    }
}

void em_A_integral(em_workspace *w, double *Fint, int i, int j, double delta_t)
{
    int p, q, u, v;
    int n = w->nstate;
    double *S = w->S;
    double *Sinv = w->Sinv;
    double *Lambda = w->Lambda;
    double *D = w->D;
    double J, du, dv;

    for (p = 0; p < n; ++p) {
        for (q = 0; q < n; ++q) {
            Fint[p*n+q] = 0;
            for (u = 0; u < n; ++u) {
                du = D[u * n + u];
                for (v = 0; v < n; ++v) {
                    dv = D[v * n + v];
                    if (fabs(du - dv) < 1e-6)
                        J = delta_t * exp(dv * delta_t);
                    else
                        J = (exp(du * delta_t) - exp(dv * delta_t)) / (du - dv);
                    Fint[p*n+q] += S[p*n+u] * Sinv[u*n+i] * S[j*n+v] *
                                Sinv[v*n+q] * Lambda[q*n+q] * J;
                }
            }
        }
    }
}

void em_calculate_B(em_workspace *w, em_tree *t)
{
    int i, node, n = w->nstate;
    double *B = w->B, *L = w->L, *R = w->R;
    int *B_exp = w->B_exp;
    int *L_exp = w->L_exp;
    int *R_exp = w->R_exp;
    double new_B;
    int new_scale;

    memset(B, 0, n * sizeof(double));
    memset(B_exp, 0, n * sizeof(double));
    for (node = 0; node < w->nnode - 1; ++node) {
        for (i = 0; i < n; ++i) {
            if (em_is_tip(t, node)) {
                new_B = L[node * n + i];
                new_scale = L_exp[node];
            } else {
                new_B = L[node * n + i] * R[em_child(t, node, 0) * n + i] *
                        R[em_child(t, node, 1) * n + i];
                new_scale = L_exp[node] + R_exp[em_child(t, node, 0)] +
                            R_exp[em_child(t, node, 1)];
            }
            add_scale(&B[i], &B_exp[i], new_B, new_scale);
        }
    }
}

void em_reestimate(em_workspace *w)
{
    int i, j, n = w->nstate;
    double rowsum;
    double *Q = w->Q;
    double *Lambda = w->Lambda;
    double *A = w->A;
    int *A_exp = w->A_exp;
    int *B_exp = w->B_exp;
    double *B = w->B;
    int Q_exp, Lambda_exp;

    for (i = 0; i < n; ++i) {
        rowsum = 0;
        for (j = 0; j < n; ++j) {
            if (i != j) {
                Q_exp = A_exp[i * n + j] - A_exp[i * n + i];
                Q[i * n + j] = A[i * n + j] * pow(10, Q_exp) / A[i * n + i];
                rowsum += Q[i * n + j];
            }
        }
        Q[i * n + i] = -rowsum;
        Lambda_exp = B_exp[i] - A_exp[i * n + i];
        Lambda[i * n + i] = B[i] * pow(10, Lambda_exp) / A[i * n + i];
    }
}

void em_workspace_free(em_workspace *w)
{
    free(w->Q);
    free(w->Lambda);
    free(w->S);
    free(w->Sinv);
    free(w->D);
    free(w->pi);
    free(w->L);
    free(w->R);
    free(w->f);
    free(w->A);
    free(w->B);
    free(w->f_exp);
    free(w->L_exp);
    free(w->R_exp);
    free(w->A_exp);
    free(w->B_exp);
    free(w->tmpd);
    free(w->tmpi);
    free(w);
}

double em_get_branching_rate(const em_workspace *w, int n)
{
    return w->Lambda[n * w->nstate + n];
}

double em_get_transition_rate(const em_workspace *w, int i, int j)
{
    return w->Q[i * w->nstate + j];
}

double em_get_S(const em_workspace *w, int i, int j)
{
    return w->S[i * w->nstate + j];
}

double em_get_Sinv(const em_workspace *w, int i, int j)
{
    return w->Sinv[i * w->nstate + j];
}

double em_get_D(const em_workspace *w, int i)
{
    return w->D[i * w->nstate + i];
}

double em_get_pi(const em_workspace *w, int i)
{
    return w->pi[i];
}

double em_get_f(const em_workspace *w, int node, int i, int j)
{
    return w->f[node * w->nstate * w->nstate + i * w->nstate + j] *
           pow(10, w->f_exp[node]);
}

double em_get_L(const em_workspace *w, int node, int i)
{
    return w->L[node * w->nstate + i];
}

double em_get_R(const em_workspace *w, int node, int i)
{
    return w->R[node * w->nstate + i];
}

double em_get_A(const em_workspace *w, int i, int j)
{
    return w->A[i * w->nstate + j] * pow(10, w->A_exp[i * w->nstate + j]);
}
