#include "tree.h"

typedef struct em_workspace em_workspace;

em_workspace *em_workspace_create(int nstate, int nnode);

void em_set_parameters(em_workspace *w, const double *lambda, const double *q);

void em_step(em_workspace *w, em_tree *t);

void em_workspace_free(em_workspace *w);

// these are mostly for testing

double em_get_branching_rate(const em_workspace *w, int n);
double em_get_transition_rate(const em_workspace *w, int i, int j);
double em_get_S(const em_workspace *w, int i, int j);
double em_get_Sinv(const em_workspace *w, int i, int j);
double em_get_D(const em_workspace *w, int i);
double em_get_pi(const em_workspace *w, int i);
double em_get_f(const em_workspace *w, int node, int i, int j);
double em_get_L(const em_workspace *w, int node, int i);
double em_get_R(const em_workspace *w, int node, int i);
double em_get_A(const em_workspace *w, int i, int j);
