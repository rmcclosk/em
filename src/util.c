#include <float.h>
#include "util.h"
#include "math.h"

#define LOG_ZERO (DBL_MIN_10_EXP/2)

void hadamard (int n, const double *X, const double *Y, double alpha, double *Z)
{
    int i;
    for (i = 0; i < n; ++i)
        Z[i] = alpha * X[i] * Y[i];
}

int scale(int n, double *x)
{
    int i, nkeep = 0;
    double ilog, logsum = 0, logmax = LOG_ZERO, expt;

    for (i = 0; i < n; ++i)
        logmax = fmax(x[i] == 0 ? LOG_ZERO : log10(x[i]), logmax);

    for (i = 0; i < n; ++i) {
        if (x[i] > 0) {
            ilog = log10(x[i]);
            if (ilog - logmax > LOG_ZERO) {
                logsum += ilog;
                nkeep += 1;
            }
        }
    }
    if (nkeep == 0)
        return 0;

    expt = (int) ceil(logsum / nkeep);
    for (i = 0; i < n; ++i)
        x[i] /= pow(10, expt);
    return expt;
}

void add_scale(double *old, int *old_scale, double new, int new_scale)
{
    int ave_scale;

    if (new_scale - *old_scale > DBL_MAX_10_EXP / 2) {
        old[0] = new;
        old_scale[0] = new_scale;
        old_scale[0] += scale(1, old);
    } else if (*old_scale - new_scale <= DBL_MAX_10_EXP / 2) {
        ave_scale = (new_scale + *old_scale) / 2;
        old[0] = *old * pow(10, *old_scale - ave_scale);
        old[0] += new * pow(10, new_scale - ave_scale);
        old_scale[0] = ave_scale;
        old_scale[0] += scale(1, old);
    }
}

void fill(int n, double *x, double z)
{
    int i;
    for (i = 0; i < n; ++i)
        x[i] = z;
}
