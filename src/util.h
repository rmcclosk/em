#ifndef UTIL_H
#define UTIL_H

void hadamard(int n, const double *X, const double *Y, double alpha, double *Z);

int scale(int n, double *x);

void add_scale(double *old, int *old_scale, double new, int new_scale);

void fill(int n, double *x, double z);

#endif
