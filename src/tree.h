#ifndef TREE_H
#define TREE_H

#include <stdio.h>

typedef struct em_tree em_tree;

em_tree *em_tree_parse(FILE *f);

int em_root(const em_tree *tree);

int em_nnode(const em_tree *tree);

int em_child(const em_tree *tree, int node, int right);

int em_parent(const em_tree *tree, int node);

int em_is_tip(const em_tree *tree, int node);

double em_branch_length(const em_tree *tree, int node);

void em_tree_free(em_tree *tree);

#endif
