%{
#include <stdio.h>
#include <stdlib.h>

int yylex(void);
void yyerror(int *topology, int *size, double *branch_length, const char *str);
int yywrap(void);

int cur = 0;

%}

%union
{
    double number;
    char *string;
}
%token COLON SEMICOLON LPAREN RPAREN COMMA
%token <string> STRING
%token <number> NUMBER

%parse-param {int *topology} {int *size} {double *branch_length}

%start tree

%%

tree:
    subtree SEMICOLON;

subtree:
    node
    {
        topology[2*cur] = -1;
        topology[2*cur+1] = -1;
        size[cur] = 1;
        ++cur;
    }
    |
    LPAREN subtree COMMA subtree RPAREN node
    {
        size[cur] = size[cur-1] + size[cur-size[cur-1]-1] + 1;
        topology[2*cur+1] = cur - 1;
        topology[2*cur] = cur - 1 - size[cur-1];
        ++cur;
    }
    ;

node:
    label length;

label:
    |
    STRING;

length:
    {
        branch_length[cur] = 0;
    }
    |
    COLON NUMBER
    {
        branch_length[cur] = yylval.number;
    }
    ;

%%

void yyerror(int *topology, int *size, double *branch_length, const char *str)
{
    fprintf(stderr, "invalid Newick format or non-binary tree: %s\n", str);
}
 
int yywrap(void)
{
    return 1;
} 
