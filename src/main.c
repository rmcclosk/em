#include "tree.h"
#include "newick_parser.h"
#include "em.h"

int main(int argc, char **argv)
{
    FILE *tree_file = fopen(argv[1], "r");
    em_tree *t = em_tree_parse(tree_file);
    em_workspace *w = em_workspace_create(2, em_nnode(t));
    double lambda[2] = {1, 2};
    double q[2] = {1, 2};

    fclose(tree_file);
    em_set_parameters(w, lambda, q);
    em_step(w, t);
    fprintf(stderr, "%f\t%f\n", em_get_transition_rate(w, 0, 1), 
                                em_get_transition_rate(w, 1, 0));
    fprintf(stderr, "%f\t%f\n", em_get_branching_rate(w, 0), 
                                em_get_branching_rate(w, 1));

    em_tree_free(t);
    return 0;
}
